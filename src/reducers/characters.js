import {
  GET_CHARACTERS_REQUEST,
  GET_CHARACTERS_SUCCESS,
  GET_CHARACTERS_FAILURE,
  GET_CHARACTER_REQUEST,
  GET_CHARACTER_SUCCESS,
  GET_CHARACTER_FAILURE,
  SET_SEARCH_TERM,
  SEARCH_CHARACTERS_REQUEST,
  SEARCH_CHARACTERS_SUCCESS,
  SEARCH_CHARACTERS_FAILURE,
  CLEAR_CHARACTERS_SEARCH,
  UPDATE_CHARACTER,
} from '../actions/characters';

import { parseResultsToObject } from '../helpers/characters-helper';

const initialState = {
  collection: {},
  editedCollection: {},
  limit: 20,
  offset: 0,
  count: 0,
  total: 0,
  isFetching: false,
  currentCharacter: null,
  isCurrentCharacterFetching: false,
  searchedTerm: '',
  searchCollection: [],
  isSearching: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_CHARACTERS_REQUEST:
      return {
        ...state,
        isFetching: true,
      };

    case GET_CHARACTERS_SUCCESS:
      return {
        ...state,
        count: action.payload.data.count,
        limit: action.payload.data.limit,
        offset: action.payload.data.offset,
        total: action.payload.data.total,
        collection: parseResultsToObject(action.payload.data.results, state.editedCollection),
        isFetching: false,
      };

    case GET_CHARACTERS_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case GET_CHARACTER_REQUEST:
      return {
        ...state,
        isFetching: true,
      };

    case GET_CHARACTER_SUCCESS:
      return {
        ...state,
        currentCharacter: 
          action.payload.data && action.payload.data.results ? action.payload.data.results[0] : null,
        isFetching: false,
      };

    case GET_CHARACTER_FAILURE:
      return {
        ...state,
        isFetching: false,
      };

    case SET_SEARCH_TERM:
      return {
        ...state,
        searchedTerm: action.searchedTerm,
      };
    case SEARCH_CHARACTERS_REQUEST:
      return {
        ...state,
        isSearching: true,
      };

    case SEARCH_CHARACTERS_SUCCESS:
      return {
        ...state,
        searchCollection: [
          ...state.searchCollection,
          ...action.payload.data.results,
        ],
        isSearching: false,
      };
    case CLEAR_CHARACTERS_SEARCH:
      return {
        ...state,
        searchCollection: [],
        searchedTerm: '',
      };
    case SEARCH_CHARACTERS_FAILURE:
      return {
        ...state,
        isSearching: false,
      };
    case UPDATE_CHARACTER:
      return {
        ...state,
        currentCharacter: action.currentCharacter,
        editedCollection: {
          ...state.editedCollection,
          [action.currentCharacter.id]: {
            ...action.currentCharacter,
          }
        }
      }
    default:
      return state;
  }
}
