import { RSAA } from 'redux-api-middleware';
import md5 from 'crypto-js/md5';

export const GET_CHARACTERS_REQUEST = 'GET_CHARACTERS_REQUEST';
export const GET_CHARACTERS_SUCCESS = 'GET_CHARACTERS_SUCCESS';
export const GET_CHARACTERS_FAILURE = 'GET_CHARACTERS_FAILURE';

export const GET_CHARACTER_REQUEST = 'GET_CHARACTER_REQUEST';
export const GET_CHARACTER_SUCCESS = 'GET_CHARACTER_SUCCESS';
export const GET_CHARACTER_FAILURE = 'GET_CHARACTER_FAILURE';
export const CLEAR_CURRENT_CHARACTER = 'CLEAR_CURRENT_CHARACTER';

export const SEARCH_CHARACTERS_REQUEST = 'SEARCH_CHARACTERS_REQUEST';
export const SEARCH_CHARACTERS_SUCCESS = 'SEARCH_CHARACTERS_SUCCESS';
export const SEARCH_CHARACTERS_FAILURE = 'SEARCH_CHARACTERS_FAILURE';

export const CLEAR_CHARACTERS_SEARCH = 'CLEAR_CHARACTERS_SEARCH';
export const SET_SEARCH_TERM = 'SET_SEARCH_TERM';

export const UPDATE_CHARACTER = 'UPDATE_CHARACTER';

const formatUrl = (route, params = []) => {
  let url = `${process.env.REACT_APP_MARVEL_URL}${route}`;
  const ts = Date.now();
  const hash = md5(`${ts}${process.env.REACT_APP_MARVEL_DEV_PRIVKEY}${process.env.REACT_APP_MARVEL_DEV_PUBKEY}`);
  url += `${params.map(param => `/${param}`)}`;
  return `${url}?ts=${ts}&apikey=${process.env.REACT_APP_MARVEL_DEV_PUBKEY}&hash=${hash.toString()}`;
};

const getCharacters = (limit = 20, offset = 0, characterName) => dispatch => {
  let url = `${formatUrl('characters')}&limit=${limit}&offset=${offset}`;
  if (characterName) {
    url += `&nameStartsWith=${characterName}`;
  }

  return dispatch({
    [RSAA]: {
      endpoint: url,
      method: 'GET',
      types: ['GET_CHARACTERS_REQUEST', 'GET_CHARACTERS_SUCCESS', 'GET_CHARACTERS_FAILURE'],
    },
  });
};

const searchCharacters = (charName, limit = 20, offset = 0) => dispatch => {
  const url = `${formatUrl('characters')}&limit=${limit}&offset=${offset}&nameStartsWith=${charName}`;
  return dispatch({
    [RSAA]: {
      endpoint: url,
      method: 'GET',
      types: [
        'SEARCH_CHARACTERS_REQUEST', 'SEARCH_CHARACTERS_SUCCESS', 'SEARCH_CHARACTERS_FAILURE',
      ],
    },
  });
};

const clearCurrentCharacter = () => {
  return {
    type: CLEAR_CURRENT_CHARACTER,
  }
};

const clearCharactersSearch = () => {
  return {
    type: CLEAR_CHARACTERS_SEARCH,
  }
};

const setSearchedTerm = (term) => {
  return {
    type: SET_SEARCH_TERM,
    searchedTerm: term,
  }
};

const getCharacter = (id) => dispatch => {
  let url = `${formatUrl('characters', [id])}`;

  return dispatch({
    [RSAA]: {
      endpoint: url,
      method: 'GET',
      types: ['GET_CHARACTER_REQUEST', 'GET_CHARACTER_SUCCESS', 'GET_CHARACTER_FAILURE'],
    },
  });
};

const updateCharacter = (character) => {
  return {
    type: UPDATE_CHARACTER,
    currentCharacter: character,
  }
};

export {
  getCharacters,
  setSearchedTerm,
  searchCharacters,
  clearCharactersSearch,
  getCharacter,
  clearCurrentCharacter,
  updateCharacter,
};