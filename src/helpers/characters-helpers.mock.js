export const results = [
  {
    "id": 1011334,
    "name": "3-D Man",
    "description": "",
    "modified": "2014-04-29T14:18:17-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1011334",
  },
  {
    "id": 1017100,
    "name": "A-Bomb (HAS)",
    "description": "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
    "modified": "2013-09-18T15:54:04-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1017100",
  },
  {
    "id": 1009144,
    "name": "A.I.M.",
    "description": "AIM is a terrorist organization bent on destroying the world.",
    "modified": "2013-10-17T14:41:30-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1009144",
  },
];

export const parsedResults = {
  1011334 : {
    "id": 1011334,
    "name": "3-D Man",
    "description": "",
    "modified": "2014-04-29T14:18:17-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1011334",
  },
  1017100: {
    "id": 1017100,
    "name": "A-Bomb (HAS)",
    "description": "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
    "modified": "2013-09-18T15:54:04-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1017100",
  },
  1009144: {
    "id": 1009144,
    "name": "A.I.M.",
    "description": "AIM is a terrorist organization bent on destroying the world.",
    "modified": "2013-10-17T14:41:30-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1009144",
  },
};

export const editedResults = {
  1011334 : {
    "id": 1011334,
    "name": "New Name",
    "description": "",
    "modified": "2014-04-29T14:18:17-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1011334",
  },
  1017100: {
    "id": 1017100,
    "name": "Test New Name",
    "description": "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
    "modified": "2013-09-18T15:54:04-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1017100",
  },
  1009144: {
    "id": 1009144,
    "name": "A.I.M.",
    "description": "AIM is a terrorist organization bent on destroying the world.",
    "modified": "2013-10-17T14:41:30-0400",
    "thumbnail": {
      "path": "http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec",
      "extension": "jpg"
    },
    "resourceURI": "http://gateway.marvel.com/v1/public/characters/1009144",
  },
};
