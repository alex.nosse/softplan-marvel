import {
  normalizeName,
  characterHasImage,
  parseResultsToObject,
  mergeEditedWithResults,
} from './characters-helper';

import { results, parsedResults, editedResults } from './characters-helpers.mock';

describe('UnitTest: normalize Name', () => {
  test('given a name, replace all spaces and special characters', () => {
    const name = '3-D Man';
    expect(normalizeName(name)).toEqual('3DMan');
  });
});


describe('UnitTest: characterHasImage function', () => {
  test('given a character object, returns true if it has an image', () => {
    const char = {
      thumbnail: {
        path: 'https://someurl.com/image',
        extension: 'jpg',
      }
    };
    expect(characterHasImage(char)).toBeTruthy();
  });

  test('given a character object, returns false if it hasn\'t an image', () => {
    const char = {
      thumbnail: {
        path: 'https://someurl.com/image_not_available',
        extension: 'jpg',
      }
    };
    expect(characterHasImage(char)).toBeFalsy();
  });
});

describe('UnitTest: parseResultsToObject function', () => {
  test('given an array of results, transforms it into an object using the character id as key', () => {
    expect(parseResultsToObject(results, {})).toEqual(parsedResults);
  });
});

describe('UnitTest: mergeEditedWithResults function', () => {
  test('given an array of results, transforms it into an object using the character id as key', () => {
    const resultObject = mergeEditedWithResults(parsedResults, editedResults);
    expect(resultObject[1011334].name).toEqual(editedResults[1011334].name);
    expect(resultObject[1017100].name).toEqual(editedResults[1017100].name);
    expect(resultObject[1009144].name).toEqual(parsedResults[1009144].name);
  });
});
