export const normalizeName = (name) => {
  return name.replace(/[^0-9a-zA-Z]/g, '').replace(/[\s]/gi, '_');
};

export const characterHasImage = (character) => {
  return character && character.thumbnail && character.thumbnail.path && 
    character.thumbnail.path.indexOf('image_not_available') === -1;
};

export const parseResultsToObject = (results, editedCollection) => {
  const parsedResults = results.reduce((accumulator, item) => {
    return {
      ...accumulator,
      [item.id]: {
        ...item,
      },
    };
  }, {});

  Object.keys(parsedResults).forEach(result => {
    if (editedCollection[result]) {
      parsedResults[result] = editedCollection[result];
    }
  });

  return parsedResults;
};

export const mergeEditedWithResults = (results, editedCollection) => {
  return Object.keys(results).reduce((acc, id) => {
    const item = editedCollection[id] ? editedCollection[id] : results[id];
    return {
      ...acc, 
      [item.id] : {
        ...item,
      }
    }
  }, {});
};
