import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Header from '../../components/Header/Header';

import List from '../../views/List';
import Character from '../../views/Character';
import SearchInput from '../../components/SearchInput/SearchInput';
import SearchResults from '../../components/SearchResults/SearchResults';
import PageLoading from '../../components/PageLoading/PageLoading';
import Loading from '../../components/Loading/Loading';

const MainTemplate = props => {
  const {
    isFetching,
    searchCharacters,
    searchCollection,
    isSearching,
    clearCharactersSearch,
    setSearchedTerm,
  } = props;

  const [term, setTerm] = useState('');

  return (
    <Router>
      <Header>
        <div className="search-container">
          <SearchInput
            onSearch={(term) => {
              clearCharactersSearch();
              setTerm(term);
              searchCharacters(term);
            }}
            resetSearch={() => {
              setTerm(null);
              searchCharacters(null);
              clearCharactersSearch();
            }} />
          <SearchResults
            term={term}
            collection={searchCollection}
            isSearching={isSearching}
            onClickViewMore={() => {
              clearCharactersSearch();
              setSearchedTerm(term);
            }}
            clearSearch={() => {
              clearCharactersSearch();
            }}
          />
        </div>
      </Header>
      <div className="body-container">
        {isFetching && (
          <PageLoading isVisible={isFetching}>
            <Loading />
          </PageLoading>
        )}
        <Switch>
          <Route path="/character/:id" component={Character} />
          <Route path="/" component={List} />
        </Switch>
      </div>
    </Router>
  );
};

export default MainTemplate;
