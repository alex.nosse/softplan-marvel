import { connect } from 'react-redux';

import MainTemplate from './MainTemplate';

import { 
  searchCharacters, 
  clearCharactersSearch, 
  setSearchedTerm 
} from '../../actions/characters';

const mapStateToProps = state => {
  const { 
    searchedTerm, 
    searchCollection, 
    isSearching, 
    isFetching
  } = state.characters;
  
  return {
    searchedTerm,
    searchCollection,
    isSearching,
    isFetching,
  };
};

const mapDispatchToProps = {
  searchCharacters,
  clearCharactersSearch,
  setSearchedTerm,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainTemplate);
