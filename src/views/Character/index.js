import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Character from './Character';
import { 
  getCharacter, 
  updateCharacter,
} from '../../actions/characters';

const mapStateToProps = state => {
  const { currentCharacter } = state.characters;
  return { currentCharacter };
};

const mapDispatchToProps = {
  getCharacter,
  updateCharacter,
};

const connectedCharacter = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Character);

export default withRouter(connectedCharacter);
