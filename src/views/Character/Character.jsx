import React, { useEffect, useState } from 'react';
import CharacterContent from '../../components/CharacterContent/CharacterContent';

import edit from '../../assets/imgs/edit-icon.png';
import { characterHasImage } from '../../helpers/characters-helper';

const Character = props => {
  const { 
    getCharacter, 
    currentCharacter, 
    match,
    updateCharacter,
  } = props;

  const [isEditing, setIsEditing] = useState(false);
  const [description, setDescription] = useState('');
  const [name, setName] = useState('');

  useEffect(() => {
    window.scrollTo(0, 0);
    if (match.params.id) {
      getCharacter(match.params.id);
    }
  }, [getCharacter, match.params.id]);

  useEffect(() => {
    if (currentCharacter) {
      setDescription(currentCharacter.description);
      setName(currentCharacter.name);
    }
  }, [currentCharacter]);

  if (!currentCharacter) return null;
  
  const customCssImage = currentCharacter && !characterHasImage(currentCharacter) ? '-no-image-found' : '';
  
  return (
    <div
      className="character-container"
    >
      <h2 className="char-header">MARVEL INTELLIGENCE BULLETIN</h2>
      
      <div className="image-container">
        <div
          className={`image ${customCssImage}`}
          style={{ backgroundImage: `url("${currentCharacter.thumbnail.path}.${currentCharacter.thumbnail.extension}")` }}>
        </div>
        <div className="profile-container">
          {isEditing ? (
            <input 
              type="text" 
              id="characterName" 
              value={name} 
              onChange={e => setName(e.target.value)}
            />
          ) : (<h3>{currentCharacter.name}</h3>)}
          {!isEditing ? 
            <img 
              src={edit} 
              onClick={() => setIsEditing(true)} 
              alt={`Edit Button Icon`}
            /> : <div>&nbsp; </div>}
          {isEditing ? 
            (<>
              <textarea
                name="characterDescription" 
                id="characterDescription" 
                value={description} 
                onChange={(e) => setDescription(e.target.value)}
              >
              </textarea>
              <button
                className={`${name.length === 0 ? '-disabled' : ''}`}
                disabled={name.length === 0}
                onClick={() => {
                const char = {
                  ...currentCharacter,
                  name, 
                  description,
                };
                updateCharacter(char);
                setIsEditing(false);
              }}>Update</button>
            </>) : 
            (<span className="description">{currentCharacter.description}</span>)}
          
        </div>
      </div>
      {currentCharacter.comics && currentCharacter.comics.items.length > 0 &&
        <CharacterContent title={'Comics'} content={currentCharacter.comics.items} />
      }
      {currentCharacter.series && currentCharacter.series.items.length > 0 &&
        <CharacterContent title={'Series'} content={currentCharacter.series.items} />
      }
      {currentCharacter.stories && currentCharacter.stories.items.length > 0 &&
        <CharacterContent title={'Stories'} content={currentCharacter.stories.items} />
      }
      {currentCharacter.events && currentCharacter.events.items.length > 0 &&
        <CharacterContent title={'Events'} content={currentCharacter.events.items} />
      }
    </div>
  );
};

export default Character;