import React, { useEffect, useState } from 'react';
import queryString from 'query-string';

import Card from '../../components/Card/Card';
import { normalizeName } from '../../helpers/characters-helper';
import Pagination from '../../components/Pagination/Pagination';

const List = props => {
  const {
    collection,
    total,
    limit,
    getCharacters,
    searchedTerm,
    history,
    location,
  } = props;
  const parsedParams = queryString.parse(location.search);

  const [error, setError] = useState(null);
  const [page, setPage] = useState(parsedParams.page ? +parsedParams.page : 1);

  const getCharactersFromApi = () => {
    const term = searchedTerm ? searchedTerm : parsedParams.name;
    const _page = parsedParams.page ? parsedParams.page : 1;
    getCharacters(limit, (_page - 1) * limit, term).then(action => {
      if (action.error) {
        setError(action.payload.message);
      }

      setPage(_page);
    });
  };

  useEffect(() => {
    getCharactersFromApi();
    // eslint-disable-next-line
  }, [searchedTerm, location]);

  const goToCharacterDetails = (character) => {
    history.push(`/character/${character.id}`);
  };

  const renderCharacters = () => {
    const _collection = collection || [];
    return Object.keys(_collection)
      .map(id => {
        const char = _collection[id];
        const nName = normalizeName(char.name);
        return (<Card
          character={char}
          key={nName}
          onClick={() => {
            goToCharacterDetails(char);
          }} />);
      });
  };

  return (
    <>
      <div className="characters-container">
        {(
          <Pagination
            total={total}
            limit={limit}
            page={page}
            term={searchedTerm}
            onChangePage={(page) => {
              setPage(page);
            }}
          />
        )}
        {!error && renderCharacters()}
        {error && (<h1 className="error">{error}</h1>)}
      </div>
    </>
  );
};

export default List;
