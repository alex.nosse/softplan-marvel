import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { 
  getCharacters,
  setSearchedTerm,
} from '../../actions/characters';

import List from './List';

const mapStateToProps = state => {
  const {
    collection,
    total,
    offset,
    limit,
    isFetching,
    searchedTerm,
  } = state.characters;
  return {
    collection,
    total,
    offset,
    limit,
    isFetching,
    searchedTerm,
  };
};

const mapDispatchToProps = {
  getCharacters,
  setSearchedTerm,
};

const connectedList = connect(
  mapStateToProps,
  mapDispatchToProps,
)(List);

export default withRouter(connectedList);
