import React from 'react';

import logo from '../../assets/imgs/marvel-logo-5.png';
import { Link } from 'react-router-dom';

const Header = props => {
  const { children } = props;
  return (
    <header className="header">
      <div />
      <Link
        to={{
          pathname: '/',
        }}>
        <img src={logo} alt="Marvel Logo, redirect to home page on click" id="marvelLogo" />
      </Link>
      {children}
    </header>
  );
};

export default Header;
