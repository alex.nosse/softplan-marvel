import { connect } from 'react-redux';

import { getCharacters } from '../../actions/characters';

import Header from './Header';

const mapStateToProps = state => {
  const {
    searchedTerm,
  } = state.characters;
  return {
    searchedTerm,
  };
};

const mapDispatchToProps = {
  getCharacters,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
