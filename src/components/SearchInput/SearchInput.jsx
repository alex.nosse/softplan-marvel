import React, { useState } from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';

const SearchInput = props => {
  const { term, onSearch, resetSearch } = props;

  const [searchedTerm, setSearchedTerm] = useState(term);

  const changedSearchedTerm = _.debounce((value) => {
    if (value && value.length >= 3) {
      onSearch(value);
    }
  }, 400);

  return (
    <>
      <input
        className="search-input"
        type="text"
        value={searchedTerm}
        id="searchInput"
        onChange={e => {
          setSearchedTerm(e.target.value);
          changedSearchedTerm(e.target.value);
        }}
        placeholder="Type the character name" />
      <Link
        className="clear-search"
        onClick={() =>{
          resetSearch();
          setSearchedTerm('');
        }}
        to={{
          pathname: '/'
        }}>X</Link>
      
    </>
  );
};

export default SearchInput;
