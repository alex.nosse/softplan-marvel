import React from 'react';

const PageLoading = props => {
  const { isVisible, children } = props;

  return (
    <div className={`loading-container ${isVisible ? '-show' : ''}`}>
      <div className="backdrop" />
      {children}
    </div>
  );
};

export default PageLoading;
