import React from 'react';

const CharacterContent = props => {
  const { title, content } = props;

  return (
    <div className="character-content">
      <h4>{title}</h4>
      <hr />
      <ul>
        {content && (content.map(cont => 
          (
            <li key={cont.name}>
              {cont.name}
            </li>
          )
        ))}
      </ul>
    </div>
  );
};

export default CharacterContent;