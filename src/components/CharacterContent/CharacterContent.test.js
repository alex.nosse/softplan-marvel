import React from 'react';
import renderer from 'react-test-renderer';

import CharacterContent from './CharacterContent';
const content = {
  title: 'Comics',
  items: [
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/21366",
      "name": "Avengers: The Initiative (2007) #14"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/24571",
      "name": "Avengers: The Initiative (2007) #14 (SPOTLIGHT VARIANT)"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/21546",
      "name": "Avengers: The Initiative (2007) #15"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/21741",
      "name": "Avengers: The Initiative (2007) #16"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/21975",
      "name": "Avengers: The Initiative (2007) #17"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/22299",
      "name": "Avengers: The Initiative (2007) #18"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/22300",
      "name": "Avengers: The Initiative (2007) #18 (ZOMBIE VARIANT)"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/22506",
      "name": "Avengers: The Initiative (2007) #19"
    },
    {
      "resourceURI": "http://gateway.marvel.com/v1/public/comics/8500",
      "name": "Deadpool (1997) #44"
    },
  ],
};

describe('given: CharacterContent', () => {
  describe('when: receive an tile and content', () => {
    test('then: it should render correctly', () => {
      const snap = renderer.create(<CharacterContent title={content.title} content={content.items} />).toJSON();
      expect(snap).toMatchSnapshot();
    });
  });

  describe('when: it receive just the title', () => {
    test('then: it should not render', () => {
      const snap = renderer.create(<CharacterContent title={content.title} />).toJSON();
      expect(snap).toMatchSnapshot();
    });
  });

});