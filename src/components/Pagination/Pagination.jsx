import React, { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

const Pagination = props => {
  const {
    total,
    page,
    limit,
    onChangePage,
    term,
  } = props;
  const pageModifier = 5;
  const paginationSize = 11;
  const [pages, setPages] = useState(0);
  const [pageInit, setPageInit] = useState(0);
  const [pageEnd, setPageEnd] = useState(0);

  useEffect(() => {
    const countPages = Math.ceil(total / limit);
    setPages(countPages);
    const isLessThanZero = page - pageModifier < 0;
    const isBiggerThanPages = page + pageModifier > countPages;
    if (isLessThanZero) {
      setPageInit(0);
      setPageEnd(paginationSize > countPages ? countPages : paginationSize);
    } else if (isBiggerThanPages) {
      setPageInit(countPages - paginationSize);
      setPageEnd(countPages);
    } else {
      setPageInit(page - pageModifier);
      setPageEnd(page + pageModifier + 1);
    }
  }, [total, limit, page]);

  const createQueryParams = (page) => {
    let queryParams = `?page=${page}`;
    if (term) {
      queryParams += `&name=${term}`;
    }
    return queryParams;
  };

  const createPagination = () => {
    let array = new Array(paginationSize > pages ? pages : paginationSize).fill(0);
    return array
      .map((item, index) => pageInit + index + 1)
      .filter((item) => item <= pageEnd)
      .map(item => {
        return (
          <div className="pagination-item" key={item}>
            {
              (item !== page) ?
                (<Link
                  onClick={() => onChangePage(item)}
                  to={{
                    pathname: '/',
                    search: createQueryParams(item),
                  }}>{item}</Link>) : item
            }
          </div>
        )
      }
      );
  };
  
  if (limit > total) return null;
  return (
    <div className="pagination-container">
      {pages > 10 && page > 1 && (
        <div className="pagination-item">
          <Link
            onClick={() => onChangePage(1)}
            to={{
              pathname: '/',
              search: createQueryParams(1),
            }}>First</Link>
        </div>
      )}
      {createPagination()}
      {pages > 10 && page < pages - 1 && (
        <div className="pagination-item">
          <Link
            onClick={() => onChangePage(pages)}
            to={{
              pathname: '/',
              search: createQueryParams(pages),
            }}>Last</Link>
        </div>
      )}
    </div>
  );
};

export default Pagination;
