import React from 'react';
import { Link } from 'react-router-dom';

import { normalizeName } from '../../helpers/characters-helper';
import Loading from '../Loading/Loading';

const SearchResults = props => {
  const {
    collection,
    isSearching,
    clearSearch,
    term,
    onClickViewMore,
  } = props;
  const originalCollection = [...collection];
  return (
    <>
      {collection && collection.length > 0 && (
        <>
          <div className="backdrop" onClick={(e) => {
            clearSearch();
          }}>

          </div>
          <div className="search-results-container">
            {collection.splice(0, 10).map(char => (
              <div className="character-name" key={`${normalizeName(char.name)}`}>
                <Link
                  to={{
                    pathname: `/character/${char.id}`
                  }}>
                  {char.name}
                </Link>
              </div>)
            )
            }
            {originalCollection.length > 10 && (
              <>
                <hr />
                <div className="see-more">
                  <Link
                    onClick={() => onClickViewMore()}
                    to={{
                      pathname: '/',
                      search: `?page=1&name=${term}`,
                    }}>See more...</Link>
                </div>
              </>)
            }
          </div>
        </>)}
      {isSearching && (
        <div className="search-results-container -is-loading">
          <Loading color={'grey'} />
        </div>
      )}
    </>
  );
};

export default SearchResults;
