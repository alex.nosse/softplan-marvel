import React, { useState } from 'react';

const Tooltip = props => {
  const { description } = props;

  return (
    <div className="tooptip" style={{ backgroundImage: `url("${character.thumbnail.path}.${character.thumbnail.extension}")` }}>
      <div className="character"
      >
        <div className="header">{character.name}</div>
        <div className="body"></div>
        <div className="footer">{description ? description : '<No Description Available>'}</div>
      </div>
    </div>
  );
};

export default Tooltip;
