import React from 'react';
import { characterHasImage } from '../../helpers/characters-helper';

const Card = props => {
  const { character, onClick } = props;

  if (!character) return null;
  
  const maxLength = 100;
  const description = character.description.length > maxLength ?
    `${character.description.substring(0, maxLength)} (...)` : character.description;
  const customBackgroundCss = !characterHasImage(character) ? 'image-not-found' : '';

  return (
    <div 
      className={`card ${customBackgroundCss} card_${character.id}`} 
      style={{ backgroundImage: `url("${character.thumbnail.path}.${character.thumbnail.extension}")` }}
      onClick={() => onClick()}
    >
      <div className="character"
      >
        <div className="character-header">{character.name}</div>
        <div className="body"></div>
        <div className="character-footer">{description ? description : '<No Description Available>'}</div>
      </div>
    </div>
  );
};

export default Card;
