import React from 'react';

const Loading = (props) => {
  const { color } = props;
  return (
    <div className={`loading ${color}`} >
      <div></div><div></div><div></div><div></div>
    </div>
  );
};

export default Loading;
