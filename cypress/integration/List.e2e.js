describe('E2E Test: List Component', () => {
  context('Basic', () => {
    it('should render the loading component before the items being loaded', () => {
      cy.visit('/');
      cy.get('.loading-container').should('exist');
      cy.get('.card').should('not.exist');
      cy.wait(1000);
      cy.get('.loading-container').should('not.exist');
    });

    it('should render a list of results if type 3 or more characters into search input', () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.search-results-container').should('not.exist');
      cy.get('#searchInput').type('iron');
      cy.wait(1000);
      cy.get('.search-results-container').should('exist');
      cy.get('.search-results-container > .character-name')
        .its('length').should('gt', 0);
    });

    it('should NOT render a list of results if type less than 3 characters into search input', () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.search-results-container').should('not.exist');
      cy.get('#searchInput').type('ir');
      cy.wait(1000);
      cy.get('.search-results-container').should('not.exist');
    });
  });

  context('Using Pagination Component', () => {
    it('should render the pagination component', () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.pagination-container').should('exist');
      cy.get('.pagination-item').its('length').should('eq', 12);
      cy.get('.pagination-item').last().contains('Last');
    });

    it('should render the "First" link if the user is in another page than 1', () => {
      cy.visit('/?page=12');
      cy.wait(500);
      cy.get('.pagination-container').should('exist');
      cy.get('.pagination-item').first().contains('First');
    });

    it('should not render "Last" link if it is in the last page', () => {
      cy.visit('/?page=75');
      cy.wait(500);
      cy.get('.pagination-container').should('exist');
      cy.get('.pagination-item').last().contains('75');
    });
  });

  context('Using Search Component', () => {
    it('should render the "See more" link if the total are higher than 20', () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.search-results-container').should('not.exist');
      cy.get('.see-more').should('not.exist');
      cy.get('#searchInput').type('spid');
      cy.wait(500);
      cy.get('.search-results-container').should('exist');
      cy.get('.see-more').should('exist');
    });

    it('should load on the List View if the user clicks on the "See more"', () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.character-header').first().contains('A.I.M.');
      cy.get('.pagination-container').children().should('have.length', 12);
      cy.get('#searchInput').type('spid');
      cy.wait(1000);
      cy.get('.see-more').should('exist');
      cy.get('.see-more > a').click();
      cy.wait(2000);
      cy.get('.character-header').first().contains('Spider-Girl (Anya Corazon)');
      cy.get('.pagination-container').children().should('have.length', 2);
    });

    it(`should load on the List View if the user clicks on the "See more", 
          and pagination should not be visible if the total is less than 20`, () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.character-header').first().contains('A.I.M.');
      cy.get('.pagination-container').children().should('have.length', 12);
      cy.get('#searchInput').type('captain');
      cy.wait(1000);
      cy.get('.see-more').should('exist');
      cy.get('.see-more > a').click();
      cy.wait(1000);
      cy.get('.character-header').first().contains('Captain America');
      cy.get('.pagination-container').should('not.exist');
    });

    it(`should reset to the page 1 for all characters if the user presses X after 
          executing a search`, () => {
      cy.visit('/');
      cy.wait(500);
      cy.get('.character-header').first().contains('A.I.M.');
      cy.get('.pagination-container').children().should('have.length', 12);
      cy.get('#searchInput').type('spid');
      cy.wait(1000);
      cy.get('.see-more').should('exist');
      cy.get('.see-more > a').click();
      cy.wait(2000);
      cy.get('.character-header').first().contains('Spider-Girl (Anya Corazon)');
      cy.get('.pagination-container').children().should('have.length', 2);
      cy.get('.clear-search').click();
      cy.get('.loading-container').should('exist');
      cy.wait(1000);
      cy.get('.loading-container').should('not.exist');
      cy.wait(500);
      cy.get('.character-header').first().contains('A.I.M.');      
    });
  });



});
