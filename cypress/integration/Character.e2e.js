describe('E2E Test: Character View Component', () => {
  context('Basic', () => {
    it('should render the loading component before the items being loaded', () => {
      cy.visit('/character/1009232');
      cy.get('.loading-container').should('exist');
      cy.get('.character-container').should('not.exist');
      cy.get('.image-container').should('not.exist');
      cy.wait(1000);
      cy.get('.loading-container').should('not.exist');
      cy.get('.character-container').should('exist');
      cy.get('.image-container').should('exist');
    });
  });

  context('Edit Character', () => {
    it(`should be possible to edit the character name,
        and on the ListView the name should be New Name`, () => {
      cy.visit('/character/1009146');
      cy.get('.loading-container').should('exist');
      cy.get('.character-container').should('not.exist');
      cy.get('.image-container').should('not.exist');
      cy.wait(1000);
      cy.get('.loading-container').should('not.exist');
      cy.get('.character-container').should('exist');
      cy.get('.image-container').should('exist');
      cy.get('#characterName').should('not.exist');
      cy.get('.profile-container > h3').contains('Abomination (Emil Blonsky)');
      cy.get('.profile-container > img').click();
      cy.get('#characterName').should('exist');
      cy.get('#characterName').clear().type('New Name');
      cy.get('.profile-container > button').click();
      cy.get('#characterName').should('not.exist');
      cy.get('.profile-container > h3').contains('New Name');
      
      cy.get('#marvelLogo').click();
      cy.wait(3000);
      cy.get('.card_1009146 .character-header').contains('New Name');
    });

    it(`should be possible to edit the character description,
        and on the ListView the description should be New Description`, () => {
      cy.visit('/character/1009146');
      cy.get('.loading-container').should('exist');
      cy.get('.character-container').should('not.exist');
      cy.get('.image-container').should('not.exist');
      cy.wait(1000);
      cy.get('.loading-container').should('not.exist');
      cy.get('.character-container').should('exist');
      cy.get('.image-container').should('exist');

      cy.get('#characterDescription').should('not.exist');
      cy.get('.profile-container > .description').contains('Formerly known as Emil Blonsky');
      cy.get('.profile-container > img').click();
      cy.get('#characterDescription').should('exist');
      cy.get('#characterDescription').clear().type('New Description');
      cy.get('.profile-container > button').click();
      cy.get('#characterDescription').should('not.exist');
      cy.get('.profile-container > .description').contains('New Description');
      
      cy.get('#marvelLogo').click();
      cy.wait(2000);
      cy.get('.card_1009146 .character-footer').contains('New Description');
    });
  });

});
